import { StyleSheet } from 'react-native';

export const commonStyles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#ffffff',
    paddingTop: 50,
    paddingBottom: 40,
  },
  buttons: {
    flexDirection: 'row',
  },
});
