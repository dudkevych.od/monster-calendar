// modules
import * as React from 'react';
import { Button, View, type ViewStyle } from 'react-native';
import { DateTime } from 'luxon';
import MonsterCalendar from 'monster-calendar';

// styles
import { commonStyles } from './commonStyles';

export default function App() {
  const [marked, setMarked] = React.useState<
    null | DateTime | (null | DateTime)[]
  >([DateTime.local().plus({ months: 1, days: 1 }), null]);

  console.log(marked);

  return (
    <View style={commonStyles.wrapper}>
      <View style={commonStyles.buttons}>
        <Button
          onPress={() => {
            setMarked((prevState: any) => {
              if (prevState?.[0]) {
                return prevState[0];
              }
              return null;
            });
          }}
          title="Single"
          disabled={!Array.isArray(marked)}
        />
        <Button
          onPress={() => {
            setMarked((prevState: any) => {
              if (prevState) {
                return [prevState, null];
              }
              return [null, null];
            });
          }}
          title="Range"
          disabled={Array.isArray(marked)}
        />
      </View>
      <MonsterCalendar
        height={150}
        initialRenderCount={3}
        maxRangeMonthsCount={3}
        marked={marked}
        onPress={(value) => {
          setMarked(value);
        }}
        min={DateTime.local()}
        weekNamesTopLevel
        showWeekExtraDays
        weekStyles={{
          wrapper: {},
        }}
        weekNamesStyles={{
          wrapper: {},
          text: {},
        }}
        dayStyles={{
          wrapper: {},
          wrapperSelected: [
            { backgroundColor: 'red' },
            { backgroundColor: 'green' },
          ] as ViewStyle,
          wrapperBetween: {
            backgroundColor: 'yellow',
          },
        }}
      />
    </View>
  );
}
