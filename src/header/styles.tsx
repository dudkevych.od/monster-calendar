import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  text: {
    textTransform: 'capitalize',
  },
});
