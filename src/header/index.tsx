// modules
import * as React from 'react';
import { Text, View } from 'react-native';

// types
import type { HeaderPropTypes } from '../types/header_prop_types';

// styles
import styles from './styles';

function equalPropsCallback() {
  return true;
}

const HeaderComponent = (props: HeaderPropTypes) => {
  const { date, locale, monthHeaderNameLength, monthHeaderStyles } = props;

  const formattedText = React.useMemo(() => {
    const format = monthHeaderNameLength === 'short' ? 'MMM yyyy' : 'MMMM yyyy';
    return date.setLocale(locale).toFormat(format);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [locale]);

  return (
    <View style={monthHeaderStyles?.wrapper}>
      <Text style={[styles.text, monthHeaderStyles?.text]}>
        {formattedText}
      </Text>
    </View>
  );
};

export const Header = React.memo(HeaderComponent, equalPropsCallback);
Header.displayName = 'Header';
