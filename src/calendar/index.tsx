// modules
import * as React from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import { DateTime } from 'luxon';

// components
import { Month } from '../month';
import { WeekNames } from '../week_names';

// hooks
import useInitializeMonths from '../hooks/useInitializeMonths';
import useGenerateWeekNames from '../hooks/useGenerateWeekNames';
import useFormattedMarked from '../hooks/useFormattedMarked';
import useInitializeScrollIndex from '../hooks/useInitializeScrollIndex';
import useMaxMonthsOnReach from '../hooks/useMaxMonthsOnReach';
import useStateCallback from '../hooks/useStateCallback';
import useSubscribeOnLastMonth from '../hooks/useSubscribeOnLastMonth';
import useScrollToClosestDate from '../hooks/useScrollToClosestDate';

// helpers
import { createMonths } from '../helpers/createMonths';

// constants
import { BEHAVIOR_ON_THIRD_PRESS, MONTHS_IN_YEAR } from '../constants';

// types
import type { MonthInitialType } from '../types/month_prop_types';
import type {
  CalendarMonthsListType,
  CalendarPropTypes,
} from '../types/calendar_prop_types';

export default function MonsterCalendar(props: CalendarPropTypes) {
  const {
    height,
    initialDate = DateTime.local(),
    initialRenderCount = MONTHS_IN_YEAR,
    maxRangeMonthsCount = null,
    onReachedMonthsRenderCount = MONTHS_IN_YEAR,
    behaviorOnThirdPress = BEHAVIOR_ON_THIRD_PRESS.RANGE_OF_THE_SAME_DATES,
    marked = null,
    direction = null,
    showSixWeeks = false,
    showWeekExtraDays = false,
    locale = 'en',
    monthHeaderNameLength = 'long',
    monthHeaderStyles,
    weekNamesTopLevel = false,
    weekNamesStyles,
    weekStyles,
    onPress,
    min = null,
    dayStyles,
    hapticFeedbackEnabled = false,
  } = props;

  const calendarRef = React.useRef(null);

  const initializeData: CalendarMonthsListType = useInitializeMonths(
    initialDate,
    initialRenderCount,
    showSixWeeks,
    marked,
    direction
  );
  const formattedMarked = useFormattedMarked(marked);
  const initialScrollIndex = useInitializeScrollIndex(
    initializeData,
    formattedMarked,
    direction
  );

  const weekNames = useGenerateWeekNames(initialDate, locale);

  const [months, setMonths] = React.useState(initializeData);
  const [loading, setLoading] = useStateCallback(false);

  const lastDateInList = useSubscribeOnLastMonth(months);

  const maxMonthsOnReach = useMaxMonthsOnReach(
    months.length,
    maxRangeMonthsCount
  );

  useScrollToClosestDate(calendarRef, months, formattedMarked);

  const keyExtractor = React.useRef((_: any, index: number) => {
    return `month-${index}`;
  }).current;

  const getItemLayout = React.useRef((_: any, index: number) => ({
    length: height,
    offset: height * index,
    index,
  })).current;

  const monthHeaderDefaultProps = React.useRef({
    locale,
    monthHeaderNameLength,
    monthHeaderStyles,
  }).current;
  const weekNamesDefaultProps = React.useRef({
    weekNames,
    weekNamesStyles,
  }).current;
  const weekDefaultProps = React.useRef({
    weekStyles,
  }).current;
  const dayDefaultProps = React.useRef({
    showWeekExtraDays,
    onPress,
    min,
    dayStyles,
    hapticFeedbackEnabled,
    behaviorOnThirdPress,
  }).current;

  const onEndReached = React.useCallback(() => {
    if (loading || maxMonthsOnReach || !lastDateInList) return;

    const newDates = createMonths(
      DateTime.fromISO(lastDateInList).plus({ month: 1 }),
      onReachedMonthsRenderCount,
      showSixWeeks
    );

    setLoading(true, () => {
      setMonths((prevState) => {
        return [...prevState, ...newDates];
      });
      setLoading(false);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loading, maxMonthsOnReach]);

  const renderItem = React.useCallback(
    ({ item: month, index }: { item: MonthInitialType; index: number }) => {
      return (
        <React.Fragment key={index}>
          <Month
            month={month.days}
            init={month.init}
            height={height}
            formattedMarked={formattedMarked}
            weekNamesTopLevel={weekNamesTopLevel}
            monthHeaderDefaultProps={monthHeaderDefaultProps}
            weekNamesDefaultProps={weekNamesDefaultProps}
            weekDefaultProps={weekDefaultProps}
            dayDefaultProps={dayDefaultProps}
          />
        </React.Fragment>
      );
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formattedMarked]
  );
  return (
    <FlatList
      ref={calendarRef}
      data={months}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      getItemLayout={getItemLayout}
      bounces={false}
      showsVerticalScrollIndicator={false}
      onEndReached={onEndReached}
      onEndReachedThreshold={2}
      decelerationRate={0.98}
      ListHeaderComponent={
        weekNamesTopLevel ? (
          <WeekNames uniqKey="week-name" {...weekNamesDefaultProps} />
        ) : null
      }
      stickyHeaderIndices={weekNamesTopLevel ? [0] : undefined}
      initialScrollIndex={initialScrollIndex}
      ListFooterComponent={!maxMonthsOnReach ? <ActivityIndicator /> : null}
    />
  );
}
