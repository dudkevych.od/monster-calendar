// modules
import * as React from 'react';
import { View } from 'react-native';

// types
import type { DaySelectedPropTypes } from '../types/day_prop_types';

// styles
import styles from './styles';

function equalPropsCallback(
  prev: DaySelectedPropTypes,
  next: DaySelectedPropTypes
) {
  return (
    prev.isSelected === next.isSelected && prev.isReversed === next.isReversed
  );
}

const SelectedDecorationComponent = (props: DaySelectedPropTypes) => {
  const { isSelected, style, isReversed } = props;

  const customStyle = React.useMemo(() => {
    if (Array.isArray(style)) {
      return isReversed ? style?.[1] ?? {} : style?.[0] ?? {};
    }
    return style ?? {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isReversed]);

  return isSelected ? (
    <View style={[styles.container, styles.container_selected, customStyle]} />
  ) : null;
};

export const SelectedDecoration = React.memo(
  SelectedDecorationComponent,
  equalPropsCallback
);
SelectedDecoration.displayName = 'SelectedDecoration';
