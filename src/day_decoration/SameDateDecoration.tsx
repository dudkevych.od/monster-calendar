// modules
import * as React from 'react';
import { View } from 'react-native';

// types
import type { ViewStyle } from 'react-native';
import type { DaySameDatesPropTypes } from '../types/day_prop_types';

// styles
import styles from './styles';

function equalPropsCallback(
  prev: DaySameDatesPropTypes,
  next: DaySameDatesPropTypes
) {
  return prev.same === next.same;
}

const SameDateDecorationComponent = (props: DaySameDatesPropTypes) => {
  const { same, style } = props;

  const commonStartStyle: ViewStyle = React.useMemo(() => {
    return Array.isArray(style)
      ? { backgroundColor: style?.[0]?.backgroundColor }
      : { backgroundColor: style?.backgroundColor };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const commonEndStyle: ViewStyle = React.useMemo(() => {
    return Array.isArray(style)
      ? { backgroundColor: style?.[1]?.backgroundColor }
      : { backgroundColor: style?.backgroundColor };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return same ? (
    <View style={[styles.container, styles.container_same_date]}>
      <View style={[styles.half_same_date, commonStartStyle]} />
      <View style={[styles.half_same_date, commonEndStyle]} />
    </View>
  ) : null;
};

export const SameDateDecoration = React.memo(
  SameDateDecorationComponent,
  equalPropsCallback
);
SameDateDecoration.displayName = 'SameDateDecoration';
