// modules
import * as React from 'react';
import { View } from 'react-native';

// types
import type { ViewStyle } from 'react-native';
import type { DayBetweenPropTypes } from '../types/day_prop_types';

// styles
import styles from './styles';
import { WEEK_BORDERS } from '../constants';

function equalPropsCallback(
  prev: DayBetweenPropTypes,
  next: DayBetweenPropTypes
) {
  return (
    prev.isBetween === next.isBetween &&
    prev.isSelected === next.isSelected &&
    prev.isBetweenReversed === next.isBetweenReversed
  );
}

const BetweenDecorationComponent = (props: DayBetweenPropTypes) => {
  const { isBetween, isSelected, isBetweenReversed, border, style } = props;

  const commonWidthStyle: ViewStyle = React.useMemo(() => {
    return { width: isSelected ? '50%' : '100%' };
  }, [isSelected]);

  const commonPositionStyle: ViewStyle = React.useMemo(() => {
    return { [isBetweenReversed ? 'left' : 'right']: 0 };
  }, [isBetweenReversed]);

  const commonRightRadiusStyle: ViewStyle = React.useRef(
    border === WEEK_BORDERS.end
      ? {
          borderTopRightRadius: style?.borderRadius,
          borderBottomRightRadius: style?.borderRadius,
        }
      : {
          borderRadius: 0,
        }
  ).current;
  const commonLeftRadiusStyle: ViewStyle = React.useRef(
    border === WEEK_BORDERS.start
      ? {
          borderTopLeftRadius: style?.borderRadius,
          borderBottomLeftRadius: style?.borderRadius,
        }
      : {
          borderRadius: 0,
        }
  ).current;

  return isBetween ? (
    <View
      style={[
        styles.container,
        commonWidthStyle,
        commonPositionStyle,
        style,
        commonRightRadiusStyle,
        commonLeftRadiusStyle,
      ]}
    />
  ) : null;
};

export const BetweenDecoration = React.memo(
  BetweenDecorationComponent,
  equalPropsCallback
);
BetweenDecoration.displayName = 'BetweenDecoration';
