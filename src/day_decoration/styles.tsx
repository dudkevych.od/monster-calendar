import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    height: '100%',
  },
  container_selected: {
    width: '100%',
    right: 0,
  },
  container_same_date: {
    width: '100%',
    right: 0,
    flexDirection: 'row',
  },
  half_same_date: {
    flex: 0.5,
  },
});
