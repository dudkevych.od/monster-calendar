import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  decoration: {
    position: 'absolute',
    top: 0,
    height: '100%',
  },
});
