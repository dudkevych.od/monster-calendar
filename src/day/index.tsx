// modules
import * as React from 'react';
import { Text, TouchableOpacity } from 'react-native';

// components
import {
  BetweenDecoration,
  SameDateDecoration,
  SelectedDecoration,
} from '../day_decoration';

// hooks
import useOnPressHandle from '../hooks/useOnPressHandle';
import useDayNumberToString from '../hooks/useDayNumberToString';
import useExtraDaysDisplayCondition from '../hooks/useExtraDaysDisplayCondition';

// types
import type { DayPropTypes } from '../types/day_prop_types';
import type { ViewStyle, TextStyle } from 'react-native';

// styles
import styles from './styles';

function equalPropsCallback(prev: DayPropTypes, next: DayPropTypes) {
  return (
    prev.isSelected === next.isSelected &&
    prev.isBetween === next.isBetween &&
    prev.isReversed === next.isReversed &&
    prev.isSame === next.isSame
  );
}
const DayComponent = (props: DayPropTypes) => {
  const {
    day,
    onPress,
    behaviorOnThirdPress,
    border,
    formattedMarked,
    showWeekExtraDays,
    extra,
    min,
    dayStyles,
    hapticFeedbackEnabled,
    isSelected,
    isBetween,
    isReversed,
    isSame,
  } = props;

  const text = useDayNumberToString(day);
  const extraDayDisplayCondition = useExtraDaysDisplayCondition(
    day,
    min,
    showWeekExtraDays,
    extra
  );

  const commonVisibilityStyle: ViewStyle = React.useMemo(() => {
    return { opacity: extraDayDisplayCondition.opacity };
  }, [extraDayDisplayCondition.opacity]);
  const commonOverflowStyle: ViewStyle = React.useMemo(() => {
    return { overflow: isSame ? 'hidden' : 'visible' };
  }, [isSame]);
  const commonTextStyle: TextStyle = React.useMemo((): TextStyle => {
    switch (true) {
      case (isSelected || isSame) && !extra:
        return dayStyles?.textSelected ?? {};
      case isBetween && !isSame && !extra:
        return dayStyles?.textBetween ?? {};
      case extra:
        return dayStyles?.textExtra ?? {};
      default:
        return {} as TextStyle;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSelected, isBetween, isSame, extra]);

  const onPressHandle = useOnPressHandle(
    day,
    formattedMarked,
    behaviorOnThirdPress,
    onPress,
    hapticFeedbackEnabled
  );

  return (
    <TouchableOpacity
      onPress={onPressHandle}
      disabled={extraDayDisplayCondition.disabled || !onPress}
      activeOpacity={0.95}
      style={[
        styles.wrapper,
        dayStyles?.wrapper,
        commonVisibilityStyle,
        commonOverflowStyle,
      ]}
    >
      <BetweenDecoration
        isBetween={isBetween && !extra}
        isSelected={isSelected && !extra}
        isBetweenReversed={isReversed && !extra}
        border={border}
        style={dayStyles?.wrapperBetween}
      />
      <SelectedDecoration
        isSelected={isSelected && !extra}
        isReversed={isReversed && !extra}
        style={dayStyles?.wrapperSelected}
      />
      <SameDateDecoration
        same={isSame && !extra}
        style={dayStyles?.wrapperSelected}
      />
      <Text style={[dayStyles?.text, commonTextStyle]}>{text}</Text>
    </TouchableOpacity>
  );
};

export const Day = React.memo(DayComponent, equalPropsCallback);
Day.displayName = 'Day';
