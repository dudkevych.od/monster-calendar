// modules
import * as React from 'react';
import { View, Text } from 'react-native';

// types
import type { WeekNamesPropTypes } from '../types/week_names_prop_types';

// styles
import styles from './styles';

function equalPropsCallback() {
  return true;
}

const WeekNamesComponent = (props: WeekNamesPropTypes) => {
  const { weekNames, uniqKey, weekNamesStyles } = props;

  return (
    <View style={[styles.wrapper, weekNamesStyles?.wrapper]}>
      {weekNames.map((name) => {
        return (
          <View key={uniqKey + name} style={styles.weekday}>
            <Text style={[styles.text, weekNamesStyles?.text]}>{name}</Text>
          </View>
        );
      })}
    </View>
  );
};

export const WeekNames = React.memo(WeekNamesComponent, equalPropsCallback);
WeekNames.displayName = 'WeekNames';
