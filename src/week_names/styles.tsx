import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
  },
  weekday: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  text: {
    textTransform: 'capitalize',
  },
});
