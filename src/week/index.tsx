// modules
import * as React from 'react';
import { View } from 'react-native';

// hooks
import useIsSelected from '../hooks/useIsSelected';
import useIsBetween from '../hooks/useIsBetween';
import useIsBetweenReverse from '../hooks/useIsBetweenReverse';
import useIsSameDatesInRange from '../hooks/useIsSameDatesInRange';

// helpers
import {
  equalPropsCallbackHelper,
  weekEqualPropsCallbackInRangeStatus,
  weekEqualPropsCallbackInSingleStatus,
} from '../helpers/equalPropsCallbackHelper';

// components
import { Day } from '../day';

// types
import type { WeekPropTypes, WeekType } from '../types/week_prop_types';
import type { FormattedMarkedTypeInArray } from '../types/marked_type';

// styles
import styles from './styles';

function equalPropsCallback(prev: WeekPropTypes, next: WeekPropTypes) {
  let preventReRender: boolean = true;
  const firstDay: number = prev.week[0]?.date as number;
  const lastDay: number = prev.week[prev.week.length - 1]?.date as number;

  function rangeCallback() {
    return weekEqualPropsCallbackInRangeStatus(
      firstDay,
      lastDay,
      prev.formattedMarked as FormattedMarkedTypeInArray,
      next.formattedMarked as FormattedMarkedTypeInArray
    );
  }
  function singleCallback() {
    return weekEqualPropsCallbackInSingleStatus(
      firstDay,
      lastDay,
      prev.formattedMarked as null | string,
      next.formattedMarked as null | string
    );
  }

  preventReRender = equalPropsCallbackHelper(
    prev.formattedMarked,
    next.formattedMarked,
    {
      rangeCallback,
      singleCallback,
    }
  );

  return preventReRender;
}

const WeekComponent = (props: WeekPropTypes) => {
  const { week, dayDefaultProps, formattedMarked, weekStyles } = props;

  const isSelected = useIsSelected(formattedMarked);
  const isBetween = useIsBetween(formattedMarked);
  const isBetweenReverse = useIsBetweenReverse(formattedMarked);
  const isSameDatesInRange = useIsSameDatesInRange(formattedMarked);

  return (
    <View style={[styles.wrapper, weekStyles?.wrapper]}>
      {week.map((day: WeekType) => {
        const additionalProps = {
          isSelected: isSelected(day.date),
          isBetween: isBetween(day.date),
          isReversed: isBetweenReverse(day.date),
          isSame: isSameDatesInRange(day.date),
        };

        return (
          <React.Fragment key={day.date}>
            <Day
              day={day.date}
              border={day.border}
              formattedMarked={formattedMarked}
              extra={day.extra}
              {...dayDefaultProps}
              {...additionalProps}
            />
          </React.Fragment>
        );
      })}
    </View>
  );
};

export const Week = React.memo(WeekComponent, equalPropsCallback);
Week.displayName = 'Week';
