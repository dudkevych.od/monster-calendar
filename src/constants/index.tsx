export const MONTHS_IN_YEAR = 12;
export const MAX_YEARS = MONTHS_IN_YEAR * 10;
export const DAYS_IN_SIX_WEEKS = 42;
export const DAYS_IN_WEEK = 7;
export enum CALENDAR_TYPES {
  single = 'single',
  range = 'range',
}
export enum CALENDAR_SELECTED_POINTS {
  from = 0,
  to = 1,
}
export enum WEEK_BORDERS {
  start,
  end,
}

export enum CALENDAR_TYPE_STATUS {
  range,
  single,
  rangeToSingle,
  singleToRange,
}

export enum BEHAVIOR_ON_THIRD_PRESS {
  RANGE_OF_THE_SAME_DATES,
  RANGE_OF_FIRST_DATE,
}
