import type { ViewStyle } from 'react-native';
import type {
  DayBorderType,
  DayPropTypes,
  OmittedDayPropTypes,
} from './day_prop_types';
import type { FormattedMarkedType } from './marked_type';

export type WeekType = { date: number; border: DayBorderType; extra: boolean };
export type WeeksListType = WeekType[];

export type WeekStylesProps = {
  wrapper?: ViewStyle;
};

export type WeekPropTypes = {
  week: WeeksListType;
  dayDefaultProps: Omit<DayPropTypes, OmittedDayPropTypes>;
  formattedMarked: FormattedMarkedType;
  weekStyles?: WeekStylesProps;
};
