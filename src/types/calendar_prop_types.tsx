// types
import type { DateTime } from 'luxon';
import type { DirectionType } from './direction_type';
import type { MarkedType } from './marked_type';
import type { MonthInitialType } from './month_prop_types';
import type {
  MonthHeaderNameLength,
  MonthHeaderStyles,
} from './header_prop_types';
import type { WeekNamesStylesType } from './week_names_prop_types';
import type { CALENDAR_TYPES, CALENDAR_TYPE_STATUS } from '../constants';
import type { WeekStylesProps } from './week_prop_types';
import type { DayStylesProps } from './day_prop_types';
import type { BehaviorOnThirdPressType } from './behavior_on_third_press';
export type CalendarMonthsListType = MonthInitialType[];
export type CalendarType = CALENDAR_TYPES.single | CALENDAR_TYPES.range;
export type CalendarTypeStatus =
  | CALENDAR_TYPE_STATUS.range
  | CALENDAR_TYPE_STATUS.single
  | CALENDAR_TYPE_STATUS.rangeToSingle
  | CALENDAR_TYPE_STATUS.singleToRange;

export type CalendarPropTypes = {
  height: number;
  initialDate?: DateTime;
  initialRenderCount?: number;
  maxRangeMonthsCount?: null | number;
  onReachedMonthsRenderCount?: number;
  behaviorOnThirdPress?: BehaviorOnThirdPressType;
  marked?: MarkedType;
  direction?: DirectionType;
  showSixWeeks?: boolean;
  showWeekExtraDays?: boolean;
  locale?: string;
  monthHeaderNameLength?: MonthHeaderNameLength;
  monthHeaderStyles?: MonthHeaderStyles;
  weekNamesTopLevel?: boolean;
  weekNamesStyles?: WeekNamesStylesType;
  weekStyles?: WeekStylesProps;
  dayStyles?: DayStylesProps;
  onPress?: (value: MarkedType) => void;
  min?: null | DateTime;
  hapticFeedbackEnabled?: boolean;
};
