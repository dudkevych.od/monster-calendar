import type { DateTime } from 'luxon';
import type { TextStyle } from 'react-native';
import type { ViewStyle } from 'react-native';

export type MonthHeaderStyles = {
  wrapper?: ViewStyle;
  text?: TextStyle;
};

export type MonthHeaderNameLength = 'short' | 'long';

export type HeaderPropTypes = {
  date: DateTime;
  locale: string;
  monthHeaderStyles?: MonthHeaderStyles;
  monthHeaderNameLength: MonthHeaderNameLength;
};
