import type { CALENDAR_SELECTED_POINTS } from '../constants';

export type DirectionType =
  | null
  | CALENDAR_SELECTED_POINTS.from
  | CALENDAR_SELECTED_POINTS.to;
