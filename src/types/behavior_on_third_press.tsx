import type { BEHAVIOR_ON_THIRD_PRESS } from '../constants';

export type BehaviorOnThirdPressType =
  | BEHAVIOR_ON_THIRD_PRESS.RANGE_OF_THE_SAME_DATES
  | BEHAVIOR_ON_THIRD_PRESS.RANGE_OF_FIRST_DATE;
