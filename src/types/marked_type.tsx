import type { DateTime } from 'luxon';

export type MarkedType = null | DateTime | (null | DateTime)[];

export type FormattedMarkedTypeInArray = (null | string)[];
export type FormattedMarkedType = null | string | FormattedMarkedTypeInArray;
