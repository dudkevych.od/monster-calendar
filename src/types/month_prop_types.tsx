import type { DayPropTypes, OmittedDayPropTypes } from './day_prop_types';
import type { HeaderPropTypes } from './header_prop_types';
import type { WeekNamesPropTypes } from './week_names_prop_types';
import type { WeekPropTypes, WeeksListType } from './week_prop_types';
import type { FormattedMarkedType } from './marked_type';

export type MonthInitialType = { days: MonthType; init: string };
export type MonthType = WeeksListType[];

export type MonthPropTypes = {
  height: number;
  init: string;
  month: MonthType;
  dayDefaultProps: Omit<DayPropTypes, OmittedDayPropTypes>;
  monthHeaderDefaultProps: Omit<HeaderPropTypes, 'date'>;
  weekNamesDefaultProps: Omit<WeekNamesPropTypes, 'uniqKey'>;
  weekNamesTopLevel: boolean;
  weekDefaultProps: Omit<
    WeekPropTypes,
    'week' | 'formattedMarked' | 'dayDefaultProps'
  >;
  formattedMarked: FormattedMarkedType;
};
