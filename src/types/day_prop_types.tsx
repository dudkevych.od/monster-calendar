import type { DateTime } from 'luxon';
import type { ViewStyle, TextStyle } from 'react-native';
import type { WEEK_BORDERS } from '../constants';
import type { FormattedMarkedType, MarkedType } from './marked_type';
import type { BehaviorOnThirdPressType } from './behavior_on_third_press';

export type OmittedDayPropTypes =
  | 'day'
  | 'border'
  | 'formattedMarked'
  | 'extra'
  | 'isSelected'
  | 'isBetween'
  | 'isReversed'
  | 'isSame';

export type DayBorderType = null | WEEK_BORDERS.start | WEEK_BORDERS.end;
export type DayStylesProps = {
  wrapper?: ViewStyle;
  wrapperSelected?: ViewStyle;
  wrapperBetween?: ViewStyle;
  text?: TextStyle;
  textSelected?: TextStyle;
  textBetween?: TextStyle;
  textExtra?: TextStyle;
};

export type DaySelectedPropTypes = {
  isSelected: boolean;
  isReversed: boolean;
  style?: ViewStyle;
};
export type DayBetweenPropTypes = {
  isBetween: boolean;
  isSelected: boolean;
  isBetweenReversed: boolean;
  border: DayBorderType;
  style?: ViewStyle;
};
export type DaySameDatesPropTypes = {
  same: boolean;
  style?: ViewStyle;
};

export type DayPropTypes = {
  day: number;
  border: DayBorderType;
  showWeekExtraDays: boolean;
  onPress?: (value: MarkedType) => void;
  behaviorOnThirdPress: BehaviorOnThirdPressType;
  formattedMarked: FormattedMarkedType;
  isSelected: boolean;
  isBetween: boolean;
  isReversed: boolean;
  isSame: boolean;
  extra: boolean;
  min: null | DateTime;
  dayStyles?: DayStylesProps;
  hapticFeedbackEnabled?: boolean;
};
