import type { TextStyle } from 'react-native';
import type { ViewStyle } from 'react-native';

type WeekNamesTextTransformStyles = {
  'text-transform'?: 'capitalize' | 'uppercase' | 'lowercase';
};

export type WeekNamesStylesType = {
  wrapper?: ViewStyle;
  text?: TextStyle & WeekNamesTextTransformStyles;
};

export type WeekNamesPropTypes = {
  weekNames: string[];
  uniqKey: string;
  weekNamesStyles?: WeekNamesStylesType;
};
