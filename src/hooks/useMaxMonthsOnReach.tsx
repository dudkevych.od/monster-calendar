// modules
import * as React from 'react';

// contants
import { MAX_YEARS } from '../constants';

export default function (
  length: number,
  maxRangeMonthsCount: null | number
): boolean {
  return React.useMemo((): boolean => {
    return maxRangeMonthsCount
      ? length >= maxRangeMonthsCount
      : length >= MAX_YEARS;
  }, [maxRangeMonthsCount, length]);
}
