import * as React from 'react';
import type { FormattedMarkedType } from '../types/marked_type';

export default function useIsSameDatesInRange(
  formattedMarked: FormattedMarkedType
) {
  return React.useCallback(
    (day: number) => {
      if (Array.isArray(formattedMarked)) {
        return (
          formattedMarked[0] === `${day}` && `${day}` === formattedMarked[1]
        );
      }
      return false;
    },
    [formattedMarked]
  );
}
