// modules
import * as React from 'react';

// helpers
import { createMonths } from '../helpers/createMonths';
import { checkInitialRenderCount } from '../helpers/checkInitialRenderCount';

// types
import type { DateTime } from 'luxon';
import type { DirectionType } from '../types/direction_type';
import type { MarkedType } from '../types/marked_type';

export default function (
  initialDate: DateTime,
  initialMonthsRenderCount: number,
  showSixWeeks: boolean,
  marked?: MarkedType,
  direction?: DirectionType
) {
  return React.useRef(
    createMonths(
      initialDate,
      checkInitialRenderCount(
        initialDate,
        initialMonthsRenderCount,
        marked,
        direction
      ),
      showSixWeeks
    )
  ).current;
}
