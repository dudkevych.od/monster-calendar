// modules
import * as React from 'react';

export default function useDayNumberToString(day: number) {
  return React.useMemo(() => {
    return day
      .toString()
      .slice(-2)
      .replace(/0(?=\d)/, '');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}
