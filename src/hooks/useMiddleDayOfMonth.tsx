// modules
import * as React from 'react';
import { DateTime } from 'luxon';

// types
import type { MonthType } from '../types/month_prop_types';
import type { WeeksListType } from '../types/week_prop_types';

export function useMiddleDateOfMonth(month: MonthType) {
  return React.useMemo(() => {
    const midWeekIndex: number = Math.floor(month.length / 2);
    const midWeek: WeeksListType = month[midWeekIndex] as WeeksListType;
    const dayOfMidWeek: string = midWeek[0]?.date.toString() as string;
    const currentYear: number = +dayOfMidWeek.slice(0, 4);
    const currentMonth: number = +dayOfMidWeek.slice(4, 6);

    return DateTime.local(currentYear, currentMonth, 15);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}
