// modules
import * as React from 'react';
import { DateTime } from 'luxon';

export default function useExtraDaysDisplayCondition(
  day: number,
  min: null | DateTime,
  showWeekExtraDays: boolean,
  extra: boolean
): { disabled: boolean; opacity: 0 | 1 } {
  const displayCondition = React.useMemo((): {
    disabled: boolean;
    opacity: 0 | 1;
  } => {
    const conditionParams: { disabled: boolean; opacity: 0 | 1 } = {
      disabled: false,
      opacity: 1,
    };

    let minPressDay = min !== null ? min.toISODate({ format: 'basic' }) : null;
    if (extra || (minPressDay && +minPressDay > day)) {
      conditionParams.disabled = true;
    }

    // if ((min instanceof DateTime && min > day) || extra) {
    //   conditionParams.disabled = true;
    // }

    if (!showWeekExtraDays && extra) {
      conditionParams.opacity = 0;
    }

    return conditionParams;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return displayCondition;
}
