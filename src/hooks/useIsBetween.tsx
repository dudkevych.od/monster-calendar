// modules
import * as React from 'react';

// types
import type { FormattedMarkedType } from '../types/marked_type';

export default function useIsBetween(formattedMarked: FormattedMarkedType) {
  return React.useCallback(
    (day: number) => {
      if (
        formattedMarked !== null &&
        Array.isArray(formattedMarked) &&
        formattedMarked.every((el) => el)
      ) {
        const start = formattedMarked[0] as string;
        const end = formattedMarked[1] as string;
        return day >= +start && day <= +end;
      }
      return false;
    },
    [formattedMarked]
  );
}
