// modules
import * as React from 'react';

// constants
import { DAYS_IN_WEEK } from '../constants';

// types
import type { DateTime } from 'luxon';

export default function useGenerateWeekNames(
  initialDate: DateTime,
  locale: string
): string[] {
  return React.useMemo(() => {
    const names: string[] = [];
    const numWeekday = initialDate.weekday;
    for (let i = 1; i <= DAYS_IN_WEEK; i++) {
      const date = DAYS_IN_WEEK + i - numWeekday - DAYS_IN_WEEK;
      const weekname = initialDate.plus({ day: date }).setLocale(locale)
        .weekdayShort as string;
      names.push(weekname);
    }
    return names;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}
