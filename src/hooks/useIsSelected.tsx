// modules
import * as React from 'react';

// types
import type { FormattedMarkedType } from '../types/marked_type';

export default function useIsSelected(formattedMarked: FormattedMarkedType) {
  return React.useCallback(
    (day: number): boolean => {
      if (formattedMarked !== null) {
        if (Array.isArray(formattedMarked)) {
          return formattedMarked.some((el) => day === +(el as string));
        }
        return day === +formattedMarked;
      }
      return false;
    },
    [formattedMarked]
  );
}
