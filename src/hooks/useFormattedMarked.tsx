// modules
import * as React from 'react';
import { DateTime } from 'luxon';

// types
import type { MarkedType } from '../types/marked_type';
import { EventEmitter } from '../services';

export default function useFormattedMarked(marked: MarkedType) {
  return React.useMemo(() => {
    let value: null | string | (null | string)[] = null;
    if (Array.isArray(marked)) {
      const markedArr: (null | string)[] = [null, null];

      if (marked[0] !== null) {
        markedArr[0] = marked[0]?.toISODate({
          format: 'basic',
        }) as string;
      }

      if (marked[1] !== null) {
        markedArr[1] = marked[1]?.toISODate({
          format: 'basic',
        }) as string;
      }

      value = markedArr;
    } else {
      if (marked !== null) {
        const date = marked as DateTime;
        value = date.toISODate({ format: 'basic' });
      } else {
        value = null;
      }
    }

    EventEmitter.emit('formatted', value);
    return value;
  }, [marked]);
}
