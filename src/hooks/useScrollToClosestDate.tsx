// modules
import * as React from 'react';

// helpers
import binaryFindIndex from '../helpers/binaryFindIndex';

// types
import type { MonthInitialType } from '../types/month_prop_types';
import type { CalendarMonthsListType } from '../types/calendar_prop_types';
import type { FormattedMarkedType } from '../types/marked_type';

export default function useScrollToClosestDate(
  calendarRef: { current: any },
  months: CalendarMonthsListType,
  formattedMarked: FormattedMarkedType
) {
  const prevFormattedMarked = React.useRef(formattedMarked);
  React.useEffect(() => {
    if (
      !!formattedMarked &&
      !Array.isArray(formattedMarked) &&
      Array.isArray(prevFormattedMarked.current)
    ) {
      const sliceHandleToNumber = (str: string) => +str.slice(0, -2);
      const numbers: number[] = months.map((el: MonthInitialType) =>
        sliceHandleToNumber(el.init)
      );

      const index = binaryFindIndex(
        numbers,
        sliceHandleToNumber(formattedMarked as string)
      );

      if (index >= 0) {
        calendarRef.current?.scrollToIndex?.({ index });
      }
    }

    prevFormattedMarked.current = formattedMarked;
  }, [formattedMarked]);
}
