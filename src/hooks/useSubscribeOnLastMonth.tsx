// modules
import * as React from 'react';

// types
import type { CalendarMonthsListType } from '../types/calendar_prop_types';

export default function useSubscribeOnLastMonth(
  months: CalendarMonthsListType
) {
  const lastDateRef = React.useRef<string | null>(null);

  React.useMemo(() => {
    lastDateRef.current = months[months.length - 1]?.init ?? null;
  }, [months]);

  return lastDateRef.current;
}
