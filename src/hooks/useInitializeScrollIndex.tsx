// modules
import * as React from 'react';

// helpers
import binaryFindIndex from '../helpers/binaryFindIndex';

// constants
import { CALENDAR_SELECTED_POINTS } from '../constants';

// types
import type { FormattedMarkedType } from '../types/marked_type';
import type { DirectionType } from '../types/direction_type';
import type { MonthInitialType } from '../types/month_prop_types';

export default function useInitializeScrollIndex(
  data: MonthInitialType[],
  formattedMarked: FormattedMarkedType,
  direction: DirectionType
) {
  return React.useRef(() => {
    const sliceHandleToNumber = (str: string) => +str.slice(0, -2);

    const numbers: number[] = data.map((el: MonthInitialType) =>
      sliceHandleToNumber(el.init)
    );
    let index = 0;
    if (Array.isArray(formattedMarked)) {
      switch (true) {
        case direction === CALENDAR_SELECTED_POINTS.from:
          if (formattedMarked?.[0] !== null) {
            index = binaryFindIndex(
              numbers,
              sliceHandleToNumber(formattedMarked[0] as string)
            );
          } else if (formattedMarked?.[1] !== null) {
            index = binaryFindIndex(
              numbers,
              sliceHandleToNumber(formattedMarked[1] as string)
            );
          }
          break;
        default:
          if (formattedMarked?.[1] !== null) {
            index = binaryFindIndex(
              numbers,
              sliceHandleToNumber(formattedMarked[1] as string)
            );
          } else if (formattedMarked?.[0] !== null) {
            index = binaryFindIndex(
              numbers,
              sliceHandleToNumber(formattedMarked[0] as string)
            );
          }
          break;
      }
    } else {
      index =
        formattedMarked !== null
          ? binaryFindIndex(
              numbers,
              sliceHandleToNumber(formattedMarked as string)
            )
          : index;
    }
    return index;
  }).current();
}
