import * as React from 'react';

export default function (initialState: any) {
  const [state, setState] = React.useState(initialState);
  const cbRef = React.useRef<null | ((payload: any) => void)>(null);

  const setStateCallback = React.useCallback(
    (currentState: any, cb?: (payload: any) => void) => {
      cbRef.current = cb ?? null;
      setState(currentState);
    },
    []
  );

  React.useEffect(() => {
    if (cbRef.current) {
      cbRef.current(state);
      cbRef.current = null;
    }
  }, [state]);

  return [state, setStateCallback];
}
