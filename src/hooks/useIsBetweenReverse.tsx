// modules
import * as React from 'react';

// types
import type { FormattedMarkedType } from '../types/marked_type';

export default function useIsBetweenReverse(
  formattedMarked: FormattedMarkedType
) {
  return React.useCallback(
    (day: number) => {
      if (
        formattedMarked !== null &&
        Array.isArray(formattedMarked) &&
        formattedMarked.every((el) => el)
      ) {
        const end = formattedMarked[1] as string;
        return day === +end;
      }
      return false;
    },
    [formattedMarked]
  );
}
