// modules
import * as React from 'react';

// helpers
import { toDateFromISO } from '../helpers/toDateFromISO';
import { hapticHandle } from '../helpers/hapticHandle';

// services
import { EventEmitter } from '../services';

// constants
import { BEHAVIOR_ON_THIRD_PRESS } from '../constants';

// types
import type { FormattedMarkedType, MarkedType } from '../types/marked_type';
import type { BehaviorOnThirdPressType } from '../types/behavior_on_third_press';

export default function useOnPressHandle(
  day: number,
  formattedMarked: FormattedMarkedType,
  behaviorOnThirdPress: BehaviorOnThirdPressType,
  onPress?: (value: MarkedType) => void,
  hapticFeedbackEnabled?: boolean
) {
  const formattedRef = React.useRef(formattedMarked);
  const preparedDayString = React.useRef(() => {
    const str = day.toString();
    return `${str.slice(0, 4)}-${str.slice(4, 6)}-${str.slice(6)}`;
  }).current();

  const date = React.useMemo(() => {
    return toDateFromISO(preparedDayString);
  }, [preparedDayString]);

  React.useEffect(() => {
    EventEmitter.on('formatted', onChangeFormatted);
    return () => {
      EventEmitter.removeEventListener('formatted');
    };
  }, []);

  function onChangeFormatted(value: FormattedMarkedType) {
    formattedRef.current = value;
  }

  function rangeOfTheSameDatesHandle(): MarkedType {
    const a = formattedRef.current?.[0] as null | string;
    const b = formattedRef.current?.[1] as null | string;

    const bothDatesSetted = !!a && !!b;
    const theSame = a === b;

    if (bothDatesSetted && !theSame) {
      /**
       * there are both dates setted
       * and the same
       * next press will set:
       * the date for both elements of array
       */
      return [date, date];
    } else {
      /**
       * @if there is "a" date:
       * and @if pressed day less then "a"
       * set: the date for both elements of array
       * @else set only "b"
       * @else
       * set "a" date
       */
      if (a) {
        if (day < +a) {
          return [date, date];
        }
        return [toDateFromISO(a), date];
      } else {
        return [date, null];
      }
    }
  }

  function rangeOfFirstDateHandle(): MarkedType {
    const a = formattedRef.current?.[0] as null | string;
    const b = formattedRef.current?.[1] as null | string;

    if (!!a && !b && day >= +a) {
      /**
       * @if there isn`t "b"
       * and pressed date greater or equal "a"
       * set "b" date
       * @else
       * set always "a"
       */
      return [toDateFromISO(a), date];
    } else {
      return [date, null];
    }
  }

  return React.useCallback(() => {
    let returnedPayload: MarkedType = null;
    if (Array.isArray(formattedRef.current)) {
      if (
        behaviorOnThirdPress === BEHAVIOR_ON_THIRD_PRESS.RANGE_OF_THE_SAME_DATES
      ) {
        returnedPayload = rangeOfTheSameDatesHandle();
      }

      if (
        behaviorOnThirdPress === BEHAVIOR_ON_THIRD_PRESS.RANGE_OF_FIRST_DATE
      ) {
        returnedPayload = rangeOfFirstDateHandle();
      }
    } else {
      returnedPayload = date;
    }

    if (hapticFeedbackEnabled) {
      hapticHandle();
    }
    onPress?.(returnedPayload);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}
