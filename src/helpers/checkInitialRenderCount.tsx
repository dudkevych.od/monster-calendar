// modules
import { DateTime } from 'luxon';

// helpers
import { getAmountMonths } from './getAmountMonths';

// constants
import { CALENDAR_SELECTED_POINTS } from '../constants';

// types
import type { MarkedType } from '../types/marked_type';
import type { DirectionType } from '../types/direction_type';

export function checkInitialRenderCount(
  initialDate: DateTime,
  initialMonthsRenderCount: number,
  marked?: MarkedType,
  direction?: DirectionType
) {
  if (marked !== null) {
    if (Array.isArray(marked)) {
      switch (true) {
        case direction === CALENDAR_SELECTED_POINTS.from:
          if (marked?.[0] !== null) {
            initialMonthsRenderCount = getAmountMonths(
              initialDate,
              marked?.[0] as DateTime,
              initialMonthsRenderCount
            );
          } else if (marked?.[1] !== null) {
            initialMonthsRenderCount = getAmountMonths(
              initialDate,
              marked[1] as DateTime,
              initialMonthsRenderCount
            );
          }
          break;
        default:
          if (marked?.[1] !== null) {
            initialMonthsRenderCount = getAmountMonths(
              initialDate,
              marked[1] as DateTime,
              initialMonthsRenderCount
            );
          } else if (marked?.[0] !== null) {
            initialMonthsRenderCount = getAmountMonths(
              initialDate,
              marked[0] as DateTime,
              initialMonthsRenderCount
            );
          }
          break;
      }
    } else {
      initialMonthsRenderCount = getAmountMonths(
        initialDate,
        marked as DateTime,
        initialMonthsRenderCount
      );
    }
  }

  return initialMonthsRenderCount;
}
