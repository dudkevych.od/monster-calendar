import type { FormattedMarkedTypeInArray } from '../types/marked_type';

export function someMonthEqualInit(
  formattedMarked: FormattedMarkedTypeInArray,
  init: number
): boolean {
  return formattedMarked.some((date) =>
    date !== null ? +date.slice(0, -2) === init : false
  );
}
