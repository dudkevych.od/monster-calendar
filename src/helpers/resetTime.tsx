// types
import type { DateTime } from 'luxon';

export default function resetTime(date: DateTime): DateTime {
  return date.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
}
