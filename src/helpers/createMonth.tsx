// helpers
import createDays from './createDays';

// types
import type { DateTime } from 'luxon';
import type { MonthInitialType } from '../types/month_prop_types';

export default function createMonth(
  date: DateTime,
  index: number,
  showSixWeeks: boolean
): MonthInitialType {
  const currentMonth = date.plus({
    month: index,
  });

  return {
    days: createDays(currentMonth, showSixWeeks),
    init: currentMonth.toISODate({ format: 'basic' }) as string,
  };
}
