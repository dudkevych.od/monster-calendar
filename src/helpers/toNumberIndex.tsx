// types
import type { DateTime } from 'luxon';

export default function toNumberIndex(date: DateTime): number {
  return Number(date.toISODate({ format: 'basic' }));
}
