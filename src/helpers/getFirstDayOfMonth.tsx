// modules
import { DateTime } from 'luxon';

// helpers
import resetTime from './resetTime';

export function getFirstDayOfMonth(initialDate: DateTime): DateTime {
  const currentYear: number = initialDate.year;
  const currentMonth: number = initialDate.month;

  return resetTime(DateTime.local(currentYear, currentMonth, 1));
}
