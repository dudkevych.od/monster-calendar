import type { DateTime } from 'luxon';

function sameDate(
  unit: 'day' | 'month' | 'year',
  a: DateTime,
  b: DateTime
): boolean {
  return a.hasSame(b, unit);
}

export { sameDate };
