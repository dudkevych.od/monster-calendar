import { trigger } from 'react-native-haptic-feedback';

const options = {
  enableVibrateFallback: true,
  ignoreAndroidSystemSettings: false,
};

export function hapticHandle(param = 'impactMedium') {
  trigger(param, options);
}
