import { CALENDAR_TYPE_STATUS } from '../constants';
import type { CalendarTypeStatus } from '../types/calendar_prop_types';
import type { FormattedMarkedType } from '../types/marked_type';

export function checkCalendarTypeStatus(
  prev: FormattedMarkedType,
  next: FormattedMarkedType
): CalendarTypeStatus {
  switch (true) {
    case Array.isArray(prev) && Array.isArray(next):
      return CALENDAR_TYPE_STATUS.range;
    case Array.isArray(prev) && !Array.isArray(next):
      return CALENDAR_TYPE_STATUS.rangeToSingle;
    case !Array.isArray(prev) && Array.isArray(next):
      return CALENDAR_TYPE_STATUS.singleToRange;
    default:
      return CALENDAR_TYPE_STATUS.single;
  }
}
