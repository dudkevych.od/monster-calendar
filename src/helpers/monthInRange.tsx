import type { FormattedMarkedTypeInArray } from '../types/marked_type';

export function monthInRange(
  formattedMarked: FormattedMarkedTypeInArray,
  init: number
): boolean {
  const prevFirst: number = +(formattedMarked[0]?.slice(0, -2) as string);
  const prevLast: number = +(formattedMarked[1]?.slice(0, -2) as string);

  return init >= prevFirst && init <= prevLast;
}
