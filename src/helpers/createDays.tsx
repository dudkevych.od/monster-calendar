// modules
import { DateTime } from 'luxon';

// helper
import { sameDate } from './sameDate';
import toNumberIndex from './toNumberIndex';

// constants
import { DAYS_IN_SIX_WEEKS, DAYS_IN_WEEK, WEEK_BORDERS } from '../constants';

// types
import type { MonthType } from '../types/month_prop_types';
import type { WeekType, WeeksListType } from '../types/week_prop_types';

export default function createDays(
  firstDayOfMonth: DateTime,
  showSixWeeks: boolean
): MonthType {
  const SIX_WEEKS: number = DAYS_IN_SIX_WEEKS / DAYS_IN_WEEK;
  const days: MonthType = Array.from({ length: SIX_WEEKS }, () => []);

  let start = firstDayOfMonth.toLocal();
  const iStart = start.weekday - 1;
  const iEnd = iStart + (start.daysInMonth as number) - 1;

  if (iStart !== 0) {
    const diff = (iStart + 1 + SIX_WEEKS) % DAYS_IN_WEEK;
    start = start.minus({ day: diff });
  }

  let firstDayOfSixthWeekIsTheCurrentMonth = false;
  if (!showSixWeeks) {
    const firstDayOfSixthWeek = start.plus({ day: 35 });
    firstDayOfSixthWeekIsTheCurrentMonth = sameDate(
      'month',
      firstDayOfMonth,
      firstDayOfSixthWeek
    );
  }

  let from = start.toMillis();
  const to = start.plus({ day: DAYS_IN_SIX_WEEKS - 1 }).toMillis();

  let i: number = 0,
    j: number = 0;

  while (from <= to) {
    const itIsSixthWeek = i + 1 >= SIX_WEEKS;
    if (
      itIsSixthWeek &&
      !showSixWeeks &&
      !firstDayOfSixthWeekIsTheCurrentMonth
    ) {
      days.pop();
      break;
    }

    const date = DateTime.fromMillis(from).toLocal();
    const isoDate = toNumberIndex(date);

    const currentArray: WeeksListType = days[i] as WeeksListType;

    let el: WeekType = {
      date: isoDate,
      border: null,
      extra: j < iStart || j > iEnd,
    };

    if (j >= iStart && j <= iEnd) {
      if (j === iStart || j % DAYS_IN_WEEK === 0) {
        el.border = WEEK_BORDERS.start;
      }

      if (j === iEnd || j % DAYS_IN_WEEK === 6) {
        el.border = WEEK_BORDERS.end;
      }
    }

    j++;

    Array.prototype.push.call(currentArray, el);
    from = date.toLocal().plus({ day: 1 }).toMillis();

    const length = days[i]?.length as number;
    if (length % DAYS_IN_WEEK === 0) {
      i++;
    }
  }

  return days;
}
