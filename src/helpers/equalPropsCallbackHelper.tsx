// helpers
import { checkCalendarTypeStatus } from './checkCalendarTypeStatus';

// constants
import { CALENDAR_TYPE_STATUS } from '../constants';

// types
import type {
  FormattedMarkedType,
  FormattedMarkedTypeInArray,
} from '../types/marked_type';
import { monthInRange } from './monthInRange';
import { someMonthEqualInit } from './someMonthEqualInit';
import { weekInRange } from './weekInRange';
type callbackType = (...args: any) => boolean;

export function equalPropsCallbackHelper(
  prev: FormattedMarkedType,
  next: FormattedMarkedType,
  {
    rangeCallback,
    singleCallback,
  }: { rangeCallback: callbackType; singleCallback: callbackType }
) {
  let preventReRender: boolean = true;
  const calendarTypeStatus = checkCalendarTypeStatus(prev, next);

  switch (calendarTypeStatus) {
    case CALENDAR_TYPE_STATUS.range:
      preventReRender = rangeCallback();
      break;
    case CALENDAR_TYPE_STATUS.single:
      preventReRender = singleCallback();
      break;
    case CALENDAR_TYPE_STATUS.rangeToSingle:
    case CALENDAR_TYPE_STATUS.singleToRange:
      preventReRender = false;
      break;
  }

  return preventReRender;
}

export function monthEqualPropsCallbackInRangeStatus(
  prevFormattedMarked: FormattedMarkedTypeInArray,
  nextFormatterMarked: FormattedMarkedTypeInArray,
  init: number
): boolean {
  let prevInThisYearMonth = false;
  let nextInThisYearMonth = false;

  if (prevFormattedMarked.every((date) => date !== null)) {
    prevInThisYearMonth = monthInRange(prevFormattedMarked, init);
  } else {
    prevInThisYearMonth = someMonthEqualInit(prevFormattedMarked, init);
  }

  if (nextFormatterMarked.every((date) => date !== null)) {
    nextInThisYearMonth = monthInRange(nextFormatterMarked, init);
  } else {
    nextInThisYearMonth = someMonthEqualInit(nextFormatterMarked, init);
  }

  return !(prevInThisYearMonth || nextInThisYearMonth);
}

export function monthEqualPropsCallbackInSingleStatus(
  prevFormattedMarked: null | string,
  nextFormatterMarked: null | string,
  init: number
): boolean {
  const prevYearMonth: number =
    prevFormattedMarked !== null ? +prevFormattedMarked?.slice(0, -2) : 0;
  const nextYearMonth: number =
    nextFormatterMarked !== null ? +nextFormatterMarked?.slice(0, -2) : 0;

  return init !== nextYearMonth && init !== prevYearMonth;
}

export function weekEqualPropsCallbackInRangeStatus(
  firstDay: number,
  lastDay: number,
  prevFormattedMarked: FormattedMarkedTypeInArray,
  nextFormatterMarked: FormattedMarkedTypeInArray
): boolean {
  const prevInThisWeek = weekInRange(firstDay, lastDay, prevFormattedMarked);
  const nextInThisWeek = weekInRange(firstDay, lastDay, nextFormatterMarked);
  return !(prevInThisWeek || nextInThisWeek);
}

export function weekEqualPropsCallbackInSingleStatus(
  firstDay: number,
  lastDay: number,
  prevFormattedMarked: null | string,
  nextFormatterMarked: null | string
): boolean {
  const prevDate: number =
    prevFormattedMarked !== null ? +prevFormattedMarked : 0;
  const nextDate: number =
    nextFormatterMarked !== null ? +nextFormatterMarked : 0;

  const prevInThisWeek = firstDay <= prevDate && prevDate <= lastDay;
  const nextInThisWeek = firstDay <= nextDate && nextDate <= lastDay;

  return !(prevInThisWeek || nextInThisWeek);
}
