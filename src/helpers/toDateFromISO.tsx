import { DateTime } from 'luxon';

export function toDateFromISO(str: string): DateTime {
  return DateTime.fromISO(str);
}
