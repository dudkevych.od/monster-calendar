// constants
import { MONTHS_IN_YEAR } from '../constants';

// types
import type { DateTime } from 'luxon';

export function getAmountMonths(
  start: DateTime,
  end: DateTime,
  initCount: number
): number {
  const diff = end.diff(start, ['months']).toObject() as {
    months: number;
  };

  const roundedDiff = +diff.months.toFixed(0);

  if (roundedDiff >= initCount) {
    initCount = roundedDiff + MONTHS_IN_YEAR / 2;
  } else if (initCount % roundedDiff <= 3) {
    initCount += MONTHS_IN_YEAR / 2;
  }

  return initCount;
}
