import type { FormattedMarkedTypeInArray } from '../types/marked_type';

export function weekInRange(
  first: number,
  last: number,
  formattedMarked: FormattedMarkedTypeInArray
): boolean {
  const firstFormatted: number = formattedMarked[0]
    ? (+formattedMarked[0] as number)
    : 0;
  const lastFormatted: number = formattedMarked[1]
    ? (+formattedMarked[1] as number)
    : 0;

  const firstFormattedInRangeOfWeek =
    first <= firstFormatted && firstFormatted <= last;
  const lastFormattedInRangeOfWeek =
    first <= lastFormatted && lastFormatted <= last;
  const daysBetweenFirstAndLastFormatted =
    first >= firstFormatted && lastFormatted >= last;

  return (
    firstFormattedInRangeOfWeek ||
    lastFormattedInRangeOfWeek ||
    daysBetweenFirstAndLastFormatted
  );
}
