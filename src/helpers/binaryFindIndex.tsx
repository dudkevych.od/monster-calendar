export default function binaryFindIndex(arr: number[], el: number): number {
  if (!arr.length || !el) return -1;

  let l = 0,
    r = arr.length - 1;

  while (l <= r) {
    const mid: number = Math.ceil((l + r) / 2);

    if (arr[mid] === el) return mid;

    if ((arr[mid] as number) < el) {
      l = mid + 1;
    } else {
      r = mid - 1;
    }
  }

  return -1;
}
