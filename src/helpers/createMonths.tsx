// helpers
import createMonth from './createMonth';

// types
import { DateTime } from 'luxon';
import type { MonthInitialType } from '../types/month_prop_types';
import type { CalendarMonthsListType } from '../types/calendar_prop_types';
import { getFirstDayOfMonth } from './getFirstDayOfMonth';

export function createMonths(
  initialDate: DateTime,
  initialMonthsRenderCount: number,
  showSixWeeks: boolean
): CalendarMonthsListType {
  const months = [];

  const firstDay: DateTime = getFirstDayOfMonth(initialDate);

  for (let i = 0; i < initialMonthsRenderCount; i++) {
    const month: MonthInitialType = createMonth(firstDay, i, showSixWeeks);

    months.push(month);
  }

  return months;
}
