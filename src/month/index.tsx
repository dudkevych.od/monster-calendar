// modules
import * as React from 'react';
import { View } from 'react-native';

// hooks
import { useMiddleDateOfMonth } from '../hooks/useMiddleDayOfMonth';

// helpers
import toNumberIndex from '../helpers/toNumberIndex';
import {
  equalPropsCallbackHelper,
  monthEqualPropsCallbackInRangeStatus,
  monthEqualPropsCallbackInSingleStatus,
} from '../helpers/equalPropsCallbackHelper';

// components
import { Week } from '../week';
import { WeekNames } from '../week_names';
import { Header } from '../header';

// types
import type { MonthPropTypes } from '../types/month_prop_types';
import type { FormattedMarkedTypeInArray } from '../types/marked_type';

function equalPropsCallback(prev: MonthPropTypes, next: MonthPropTypes) {
  let preventReRender: boolean = true;
  const initYearMonth = +prev.init.slice(0, -2);

  function rangeCallback() {
    return monthEqualPropsCallbackInRangeStatus(
      prev.formattedMarked as FormattedMarkedTypeInArray,
      next.formattedMarked as FormattedMarkedTypeInArray,
      initYearMonth
    );
  }
  function singleCallback() {
    return monthEqualPropsCallbackInSingleStatus(
      prev.formattedMarked as null | string,
      next.formattedMarked as null | string,
      initYearMonth
    );
  }

  preventReRender = equalPropsCallbackHelper(
    prev.formattedMarked,
    next.formattedMarked,
    {
      rangeCallback,
      singleCallback,
    }
  );

  return preventReRender;
}

const MonthComponent = (props: MonthPropTypes) => {
  const {
    height,
    month,
    dayDefaultProps,
    monthHeaderDefaultProps,
    weekNamesDefaultProps,
    weekDefaultProps,
    weekNamesTopLevel,
    formattedMarked,
  } = props;

  const stylesDefault = React.useRef({ height }).current;
  const middleDayOfMonth = useMiddleDateOfMonth(month);

  return (
    <View style={stylesDefault}>
      <Header date={middleDayOfMonth} {...monthHeaderDefaultProps} />
      {!weekNamesTopLevel && (
        <WeekNames
          uniqKey={toNumberIndex(middleDayOfMonth).toString()}
          {...weekNamesDefaultProps}
        />
      )}
      {month.map((week) => {
        return (
          <React.Fragment key={week[0]?.date}>
            <Week
              week={week}
              formattedMarked={formattedMarked}
              dayDefaultProps={dayDefaultProps}
              {...weekDefaultProps}
            />
          </React.Fragment>
        );
      })}
    </View>
  );
};

export const Month = React.memo(MonthComponent, equalPropsCallback);
Month.displayName = 'Month';
